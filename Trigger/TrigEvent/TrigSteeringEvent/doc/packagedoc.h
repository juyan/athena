/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

/**


@page TrigSteeringEvent_page TrigSteeringEvent Package

@section TrigSteeringEvent_TrigSteeringEventIntro Introduction

This package contains utilisties used to stream PESA results. 
 - HLT::HLTResult:
 - TriggerDecision:
 - LVL1CTP::Lvl1result:

Here reside also the classe used in the seeding process which holds eta/phi of RoIs: 
 - TrigRoiDescriptor:
 - HLT::ErrorCode: which defines basically possible error codes
 
And some other misc helpers:
 - MessageSvcProvider: 

Notice little dependency. It is realy a core package.


*/
