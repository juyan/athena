#include "../InDetBeamSpotFinder.h"
#include "../InDetBeamSpotRooFit.h"
#include "../InDetBeamSpotVertex.h"
#include "../RefitTracksAndVertex.h"


DECLARE_COMPONENT( InDet::InDetBeamSpotFinder )
DECLARE_COMPONENT( InDet::InDetBeamSpotVertex )
DECLARE_COMPONENT( InDet::InDetBeamSpotRooFit )
DECLARE_COMPONENT( RefitTracksAndVertex )

